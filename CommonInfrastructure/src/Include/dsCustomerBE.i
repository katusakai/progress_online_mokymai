
/*------------------------------------------------------------------------
    File        : dsCustomerBE.i
    Purpose     : 

    Syntax      :

    Description : 

    Author(s)   : 
    Created     : Thu Oct 03 15:37:43 EEST 2019
    Notes       :
  ----------------------------------------------------------------------*/

/* ***************************  Definitions  ************************** */

{include/ttCustomer.i {&ClassAccess}}
{include/ttOrder.i {&ClassAccess}}
{include/ttOrderLine.i {&ClassAccess}}

DEFINE {&ClassAccess} DATASET dsCustomerBE for
ttCustomer, ttOrder, ttOrderLine
    DATA-RELATION CustomerOrders for ttCustomer, ttOrder
    RELATION-FIELDS (Custnum, CustNum)
    DATA-RELATION OrderOrderLines for ttOrder, ttOrderLine
    RELATION-FIELDS (Ordernum, OrderNum)
.

/* ********************  Preprocessor Definitions  ******************** */


/* ***************************  Main Block  *************************** */
