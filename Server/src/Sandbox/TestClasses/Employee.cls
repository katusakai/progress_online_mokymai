 
 /*------------------------------------------------------------------------
    File        : Employee
    Purpose     : 
    Syntax      : 
    Description : 
    Author(s)   : Administrator
    Created     : Thu Oct 03 08:06:36 EEST 2019
    Notes       : 
  ----------------------------------------------------------------------*/

USING Progress.Lang.*.

BLOCK-LEVEL ON ERROR UNDO, THROW.

CLASS Sandbox.TestClasses.Employee: 

    DEFINE PUBLIC PROPERTY FirstName AS CHARACTER NO-UNDO 
    GET.
    PRIVATE SET. 

    DEFINE PUBLIC PROPERTY LastName AS CHARACTER NO-UNDO 
    GET.
    PRIVATE SET. 
    
    DEFINE PUBLIC PROPERTY JobTitle AS CHARACTER NO-UNDO 
    GET.
    PRIVATE SET. 

    DEFINE PUBLIC PROPERTY EmpNum AS INTEGER NO-UNDO 
    GET.
    PRIVATE SET. 

    DEFINE PUBLIC PROPERTY VacationHours AS INTEGER NO-UNDO 
    GET.
    SET. 

    DEFINE PUBLIC PROPERTY Address AS CHARACTER NO-UNDO 
    GET.
    SET. 

    DEFINE PUBLIC PROPERTY PostalCode AS CHARACTER NO-UNDO 
    GET.
    SET(INPUT arg AS CHARACTER):
        PostalCode = arg.
                
    END SET.

    DEFINE PUBLIC PROPERTY PhoneNumbers AS CHARACTER EXTENT 3 NO-UNDO 
    GET(INPUT idx AS INTEGER):
         RETURN PhoneNumbers[idx].
    END GET.
    SET(INPUT arg AS CHARACTER, INPUT idx AS INTEGER):
        PhoneNumbers[idx] = arg.        
    END SET.
          

    /*------------------------------------------------------------------------------
     Purpose:
     Notes:
    ------------------------------------------------------------------------------*/
        
    CONSTRUCTOR PUBLIC Employee (  ):
        SUPER ().
        
    END CONSTRUCTOR.

    /*------------------------------------------------------------------------------
     Purpose:
     Notes:
    ------------------------------------------------------------------------------*/

    METHOD PUBLIC VOID Initialize( input pFirstName as CHARACTER,
                                   input pLastName as CHARACTER,
                                   input pJobTitle as CHARACTER,
                                   input pEmpNum as INTEGER,
                                   input pVacationHours as INTEGER,
                                   input pAddress as CHARACTER,
                                   input pPostalCode as CHARACTER,
                                   input pPhoneNumbers as CHARACTER EXTENT 3
                                   ):  

        ASSIGN
            FirstName = pFirstName
            LastName = pLastName
            JobTitle = pJobTitle
            EmpNum = pEmpNum
            VacationHours = pVacationHours
            Address = pAddress
            PostalCode = pPostalCode
            PhoneNumbers[1] = pPhoneNumbers[1]
            PhoneNumbers[2] = pPhoneNumbers[2]
            PhoneNumbers[3] = pPhoneNumbers[3]
            
        .
        
        RETURN.
 
    END METHOD.

    /*------------------------------------------------------------------------------
     Purpose:
     Notes:
    ------------------------------------------------------------------------------*/

    METHOD PUBLIC VOID SetVacationHours( INPUT pHours as INTEGER ):
        
        VacationHours = pHours.
        
        RETURN.
    END METHOD.

    /*------------------------------------------------------------------------------
     Purpose:
     Notes:
    ------------------------------------------------------------------------------*/

    METHOD PUBLIC VOID SetJobTitle( INPUT pJobTitle as character ):
        
        JobTitle = pJobTitle.
        
        RETURN.
    END METHOD.

    /*------------------------------------------------------------------------------
     Purpose:
     Notes:
    ------------------------------------------------------------------------------*/

    METHOD PUBLIC CHARACTER GetInfo(  ):
        
        DEFINE VARIABLE EmpInfo AS CHARACTER NO-UNDO.
        EmpInfo = "Full Name: " + GetName() 
                + ", Address: " + Address
                + ", PostalCode: " + PostalCode
                + ", JobTitle: " + JobTitle
                + "Vacation hours: " + string(VacationHours) + " hours.".
        
        RETURN EmpInfo.
    END METHOD.

    /*------------------------------------------------------------------------------
     Purpose:
     Notes:
    ------------------------------------------------------------------------------*/

    METHOD PUBLIC CHARACTER GetName(  ):
        
        DEFINE VARIABLE FullName AS CHARACTER NO-UNDO.
        
        FullName = FirstName + " " + LastName.

        RETURN FullName.

    END METHOD.

END CLASS.