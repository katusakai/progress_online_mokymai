 
 /*------------------------------------------------------------------------
    File        : OrderLineDataSource
    Purpose     : 
    Syntax      : 
    Description : 
    Author(s)   : Administrator
    Created     : Mon Oct 07 15:42:44 EEST 2019
    Notes       : 
  ----------------------------------------------------------------------*/

USING Progress.Lang.*.
USING DataSource.IDataSource.

BLOCK-LEVEL ON ERROR UNDO, THROW.

CLASS DataSource.OrderLineDataSource IMPLEMENTS IDataSource: 
    
    DEFINE DATA-SOURCE OrderLineSource for OrderLine.
    /*------------------------------------------------------------------------------
     Purpose:
     Notes:
    ------------------------------------------------------------------------------*/
        
    CONSTRUCTOR PUBLIC OrderLineDataSource (  ):
        SUPER ().
        
    END CONSTRUCTOR.

    METHOD PUBLIC VOID Attach( INPUT phTableBuffer AS HANDLE ):
        
        phTableBuffer:ATTACH-DATA-SOURCE (DATA-SOURCE OrderLineSource:HANDLE).
        
        RETURN.
    END METHOD.

    METHOD PUBLIC VOID Attach( INPUT phTableBuffer AS HANDLE, INPUT pFilter AS CHARACTER ):
        
        phTableBuffer:ATTACH-DATA-SOURCE (DATA-SOURCE OrderLineSource:HANDLE).
        DATA-SOURCE OrderLineSource:FILL-WHERE-STRING = "where " + pFilter.
        
        RETURN.
    END METHOD.

    METHOD PUBLIC VOID Detach( INPUT phTableBuffer AS HANDLE ):
        
        phTableBuffer:DETACH-DATA-SOURCE ().
        
        RETURN.
    END METHOD.

END CLASS.