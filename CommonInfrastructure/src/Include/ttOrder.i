
/*------------------------------------------------------------------------
    File        : ttOrder.i
    Purpose     : 

    Syntax      :

    Description : 

    Author(s)   : 
    Created     : Thu Oct 03 15:03:17 EEST 2019
    Notes       :
  ----------------------------------------------------------------------*/

/* ***************************  Definitions  ************************** */
DEFINE {&ClassAccess} TEMP-TABLE ttOrder NO-UNDO
BEFORE-TABLE ttOrderBefore
    field Ordernum as integer
    field CustNum as integer
    field OrderDate as date
    field ShipDate as date
    field PromiseDate as date
    field Carrier as character
    field Instructions as character
    field PO as character
    field Terms as character
    field SalesRep as character
    field BillToID as integer
    field ShipToId as integer
    field OrderStatus as character
    field WarehouseNum as integer
    field Creditcard as character
    
    index CustOrder is unique CustNum Ordernum
    index OrderDate OrderDate
    index OrderNum is unique primary Ordernum
    index OrderStatus OrderStatus
    index SalesRep SalesRep        
.

/* ********************  Preprocessor Definitions  ******************** */


/* ***************************  Main Block  *************************** */
