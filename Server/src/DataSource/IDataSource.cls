
/*------------------------------------------------------------------------
    File        : IDataSource
    Purpose     : 
    Syntax      : 
    Description : 
    Author(s)   : Administrator
    Created     : Mon Oct 07 15:31:05 EEST 2019
    Notes       : 
  ----------------------------------------------------------------------*/

USING Progress.Lang.*.

INTERFACE DataSource.IDataSource:  

    METHOD PUBLIC VOID Attach( INPUT phTableBuffer AS HANDLE ).
    
    METHOD PUBLIC VOID Attach( INPUT phTableBuffer AS HANDLE,
                               INPUT pFilter AS CHARACTER ).

    METHOD PUBLIC VOID Detach( INPUT phTableBuffer AS HANDLE ).

END INTERFACE.