 
 /*------------------------------------------------------------------------
    File        : CustomerBE
    Purpose     : 
    Syntax      : 
    Description : 
    Author(s)   : Administrator
    Created     : Thu Oct 03 14:25:47 EEST 2019
    Notes       : 
  ----------------------------------------------------------------------*/
USING Progress.Lang.*.
USING BusinessEntity.IBussinessEntity.
USING BusinessEntity.CustomerBE FROM PROPATH.
USING OpenEdge.Net.URI FROM PROPATH.
USING System.IO.Directory FROM ASSEMBLY.
USING DataAccess.CustomerBE-DA.

BLOCK-LEVEL ON ERROR UNDO, THROW.

CLASS BusinessEntity.CustomerBE IMPLEMENTS IBussinessEntity: 
  
    {include/dsCustomerBE.i &ClassAccess = "private"}
    
    DEFINE PUBLIC STATIC PROPERTY Instance AS CustomerBE NO-UNDO 
    GET.
    PRIVATE SET. 

	/*------------------------------------------------------------------------------
	 Purpose:
	 Notes:
	------------------------------------------------------------------------------*/
	
	DEFINE PRIVATE VARIABLE DA AS CustomerBE-DA NO-UNDO.
		
	CONSTRUCTOR STATIC CustomerBE (  ):
		
		Instance = new CustomerBE().
		
	END CONSTRUCTOR.
	
	CONSTRUCTOR CustomerBE ():
	    	    
        DA = new CustomerBE-DA().
        
    END CONSTRUCTOR.

    METHOD PUBLIC VOID GetData( INPUT pFilter AS CHARACTER, OUTPUT phds AS HANDLE ):
        
        DEFINE VARIABLE Num1 AS INTEGER NO-UNDO.
        DEFINE VARIABLE Num2 AS INTEGER NO-UNDO.
        
        phds = dataset dsCustomerBE:HANDLE.
       
       IF pFilter = ""
        THEN
            DA:GetAllData(output dataset dsCustomerBE by-reference). 
        ELSE
            DA:GetCustomerData(pFilter, output dataset dsCustomerBE by-reference).
                
        
        RETURN.
    END METHOD.

    METHOD PUBLIC VOID GetOrderData( INPUT pFilter AS CHARACTER, OUTPUT phds AS HANDLE ):
        
        phds = dataset dsCustomerBE:HANDLE.
        DA:GetOrderData(pFilter, output dataset dsCustomerBE by-reference).
        
        RETURN.

    END METHOD.

    METHOD PUBLIC VOID UpdateData( INPUT-OUTPUT phdsChange AS HANDLE ):
        
        UNDO, THROW NEW Progress.Lang.AppError("METHOD NOT IMPLEMENTED").

    END METHOD.
    
    method private void ParseFilter (input pFilter as character,
        output pFirstNum as integer,
        output pSecondNum as integer):
        define variable idx as integer no-undo.
             
        /* pFilter currently looks  
           "CustNum >= n1 AND CustNum <= n2" */

        assign
            idx        = index(pFilter,">= ")
            pFilter    = substring(pFilter,idx + 3)
            idx        = index(pFilter, "AND") - 1         
            pFirstNum  = integer(substring(pFilter,1,idx))
            idx        = index(pFilter,"<= ")
            pSecondNum = integer(substring(pFilter,idx + 3))
            .
        return.
    end method.
    
END CLASS.