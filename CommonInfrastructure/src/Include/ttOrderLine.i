
/*------------------------------------------------------------------------
    File        : ttOrderLine.i
    Purpose     : 

    Syntax      :

    Description : 

    Author(s)   : 
    Created     : Thu Oct 03 15:32:24 EEST 2019
    Notes       :
  ----------------------------------------------------------------------*/

/* ***************************  Definitions  ************************** */
DEFINE {&ClassAccess} TEMP-TABLE ttOrderLine NO-UNDO
BEFORE-TABLE ttOrderLineBefore
    field Ordernum as integer
    field Linenum as integer
    field Itemnum as integer
    field Price as decimal
    field Qty as integer
    field Discount as integer
    field ExtendedPrice as decimal
    field OrderLineStatus as character
    
    index itemnum Itemnum
    index orderline is unique primary Ordernum Linenum
    index orderlinestatus OrderLineStatus
.

/* ********************  Preprocessor Definitions  ******************** */


/* ***************************  Main Block  *************************** */
