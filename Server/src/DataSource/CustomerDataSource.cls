 
 /*------------------------------------------------------------------------
    File        : CustomerDataSource
    Purpose     : 
    Syntax      : 
    Description : 
    Author(s)   : Administrator
    Created     : Mon Oct 07 15:34:47 EEST 2019
    Notes       : 
  ----------------------------------------------------------------------*/

USING Progress.Lang.*.
USING DataSource.IDataSource.

BLOCK-LEVEL ON ERROR UNDO, THROW.

CLASS DataSource.CustomerDataSource IMPLEMENTS IDataSource: 
    /*------------------------------------------------------------------------------
     Purpose:
     Notes:
    ------------------------------------------------------------------------------*/
    
    DEFINE DATA-SOURCE CustomerSource for Customer.
        
    CONSTRUCTOR PUBLIC CustomerDataSource (  ):
        SUPER ().
        
    END CONSTRUCTOR.

    METHOD PUBLIC VOID Attach( INPUT phTableBuffer AS HANDLE ):
        
        phTableBuffer:ATTACH-DATA-SOURCE (DATA-SOURCE CustomerSource:HANDLE).
        
        RETURN.
    END METHOD.

    METHOD PUBLIC VOID Attach( INPUT phTableBuffer AS HANDLE, INPUT pFilter AS CHARACTER ):
        
        phTableBuffer:ATTACH-DATA-SOURCE (DATA-SOURCE CustomerSource:HANDLE).
        data-source CustomerSource:FILL-WHERE-STRING = "where " + pFilter.

        RETURN.
    END METHOD.

    METHOD PUBLIC VOID Detach( INPUT phTableBuffer AS HANDLE ):
        
        phTableBuffer:DETACH-DATA-SOURCE ().
        
        RETURN.
    END METHOD.

END CLASS.