 
 /*------------------------------------------------------------------------
    File        : Test_CustomeBE
    Purpose     : 
    Syntax      : 
    Description : 
    Author(s)   : Administrator
    Created     : Fri Oct 04 10:54:29 EEST 2019
    Notes       : 
  ----------------------------------------------------------------------*/

USING Progress.Lang.*.
 

BLOCK-LEVEL ON ERROR UNDO, THROW.

CLASS BusinessEntity.Test_CustomerBE:
    
    {include/dsCustomerBE.i &ClassAccess = "private"}
    DEFINE PRIVATE VARIABLE hSICustomerBEProc as handle NO-UNDO.
    DEFINE PRIVATE VARIABLE hAppServerConnection AS HANDLE NO-UNDO.
    
    /*------------------------------------------------------------------------------
            Purpose:                                                                        
            Notes:                                                                        
    ------------------------------------------------------------------------------*/
   
    @Setup.
    METHOD PUBLIC VOID setUp(  ):
        
        DEFINE VARIABLE ConnectionString AS CHARACTER NO-UNDO.
        ConnectionString = "-H localhost -AppService restbroker1 -sessionModel Session-free".
        CREATE SERVER hAppServerConnection.
        hAppServerConnection:CONNECT (ConnectionString, "Training").
        RUN ServiceInterface/CustomerBE_ServiceInterfaces SINGLE-RUN ON hAppServerConnection SET hSICustomerBEProc.
                
        RETURN.
    END METHOD.
    
    /*------------------------------------------------------------------------------
            Purpose:                                                                        
            Notes:                                                                        
    ------------------------------------------------------------------------------*/

    @TearDown.
    METHOD PUBLIC VOID tearDown(  ):
        DATASET dsCustomerBE:EMPTY-DATASET ().
        DELETE PROCEDURE hSICustomerBEProc.
        hAppServerConnection:DISCONNECT ().
        DELETE OBJECT hAppServerConnection.
        
        RETURN.    
    END METHOD.
    
    @Test.
    METHOD PUBLIC VOID testGetData( ):
        OUTPUT TO "Test_CustomerBE1.out".

            DATASET dsCustomerBE:EMPTY-DATASET ().
            RUN SI_GetData in hSICustomerBEProc (input "", 
                                                output dataset dsCustomerBE).

            FOR EACH ttCustomer:
                MESSAGE ttCustomer.Custnum ttCustomer.name ttCustomer.Address SKIP.
                FOR EACH ttOrder WHERE ttOrder.CustNum = ttCustomer.Custnum:
                    MESSAGE "     " ttOrder.Ordernum ttOrder.OrderStatus SKIP.
                    FOR EACH ttOrderLine WHERE ttOrderLine.Ordernum = ttOrder.Ordernum:
                        MESSAGE "         " ttOrderLine.Linenum ttOrderLine.ExtendedPrice.
                    END.
                END.
            END.

        OUTPUT CLOSE.

        OUTPUT TO "Test_CustomerBE2.out".

            DATASET dsCustomerBE:EMPTY-DATASET ().
            RUN SI_GetData in hSICustomerBEProc (input "CustNum >= 3 AND CustNum <= 10",
                                                output dataset dsCustomerBE).

            FOR EACH ttCustomer:
                MESSAGE ttCustomer.Custnum ttCustomer.name ttCustomer.Address SKIP.
                FOR EACH ttOrder WHERE ttOrder.CustNum = ttCustomer.Custnum:
                    MESSAGE "     " ttOrder.Ordernum ttOrder.OrderStatus SKIP.
                    FOR EACH ttOrderLine WHERE ttOrderLine.Ordernum = ttOrder.Ordernum:
                        MESSAGE "         " ttOrderLine.Linenum ttOrderLine.ExtendedPrice.
                    END.
                END.
            END.

        OUTPUT CLOSE.

        OUTPUT TO "Test_CustomerBE3.out".

            DATASET dsCustomerBE:EMPTY-DATASET ().
            RUN SI_GetData in hSICustomerBEProc (input "CustNum >= 10000 AND CustNum <= 20000",
                                                output dataset dsCustomerBE).

            FOR EACH ttCustomer:
                MESSAGE ttCustomer.Custnum ttCustomer.name ttCustomer.Address SKIP.
                FOR EACH ttOrder WHERE ttOrder.CustNum = ttCustomer.Custnum:
                    MESSAGE "     " ttOrder.Ordernum ttOrder.OrderStatus SKIP.
                    FOR EACH ttOrderLine WHERE ttOrderLine.Ordernum = ttOrder.Ordernum:
                        MESSAGE "         " ttOrderLine.Linenum ttOrderLine.ExtendedPrice.
                    END.
                END.
            END.

        OUTPUT CLOSE.
        
        RETURN.
    END METHOD.
         
    @Test.
    METHOD PUBLIC VOID testUpdateData( ):

        RETURN.
    END METHOD.

    @Test.
    METHOD PUBLIC VOID testParseFilter( ):

        RETURN.
    END METHOD.
    
    @Test.
    METHOD PUBLIC VOID testGetOrderData( ):
        
        OUTPUT TO "Test_CustomerBE-order1.out".
            DATASET dsCustomerBE:EMPTY-DATASET ().
            RUN SI_GetData in hSICustomerBEProc (input "OrderNum = 25", 
                                                output dataset dsCustomerBE).
            for each ttOrder:
                message "    " ttOrder.OrderNum ttOrder.OrderStatus skip.
                for each ttOrderLine where ttOrderLine.OrderNum = ttOrder.OrderNum:
                    message "       " ttOrderLine.Linenum ttOrderLine.ExtendedPrice
                    .
                end.
            end.       
        OUTPUT CLOSE.        
                
        OUTPUT TO "Test_CustomerBE-order2.out".
            DATASET dsCustomerBE:EMPTY-DATASET ().
            RUN SI_GetData in hSICustomerBEProc (input "OrderNum = 100000", 
                                                output dataset dsCustomerBE).
            for each ttOrder:
                message "    " ttOrder.OrderNum ttOrder.OrderStatus skip.
                for each ttOrderLine where ttOrderLine.OrderNum = ttOrder.OrderNum:
                    message "       " ttOrderLine.Linenum ttOrderLine.ExtendedPrice
                    .
                end.
            end.       
        OUTPUT CLOSE.       
        
        RETURN.
    END METHOD.
END CLASS.