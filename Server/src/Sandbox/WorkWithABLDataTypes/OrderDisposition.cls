
/*------------------------------------------------------------------------
    File        : OrderDisposition
    Purpose     : 
    Syntax      : 
    Description : 
    Author(s)   : Administrator
    Created     : Mon Oct 07 08:41:10 EEST 2019
    Notes       : 
  ----------------------------------------------------------------------*/
using Progress.Lang.*.

block-level on error undo, throw.

ENUM Sandbox.WorkWithABLDataTypes.OrderDisposition:  
    DEFINE ENUM
        Confirmed
        Approved
        InStock
        BackOrdered
        Canceled
        Packaged
        Shipped
        Returned        
    .
END ENUM.