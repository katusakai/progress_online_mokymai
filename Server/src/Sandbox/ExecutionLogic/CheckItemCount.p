
/*------------------------------------------------------------------------
    File        : CheckItemCount.p
    Purpose     : 

    Syntax      :

    Description : 

    Author(s)   : 
    Created     : Mon Oct 07 13:08:00 EEST 2019
    Notes       :
  ----------------------------------------------------------------------*/

/* ***************************  Definitions  ************************** */

BLOCK-LEVEL ON ERROR UNDO, THROW.

{include/dsCustomerBE.i}

DEFINE VARIABLE TheHandle AS HANDLE NO-UNDO.
DEFINE VARIABLE ItemTotal AS INTEGER NO-UNDO.
DEFINE VARIABLE Done AS LOGICAL NO-UNDO INITIAL no.

/* ********************  Preprocessor Definitions  ******************** */


/* ***************************  Main Block  *************************** */

TheHandle = DATASET dsCustomerBE:handle.
TheHandle:READ-JSON ("file",
                    "/progress_education/openedge/abli/Exercise/Lesson04/dsCustomerBE.JSON",
                    "empty").
                    
FOR EACH ttOrderLine WHILE not Done:
    IF ttOrderLine.Itemnum = 46
        THEN DO:
        ItemTotal = ItemTotal + ttOrderLine.Qty.
    END.
    
    IF ItemTotal > 200
        THEN DO:
            MESSAGE "Exceeded 200 shipped for item 46" 
            VIEW-AS ALERT-BOX.
        Done = yes.
    END.
END.