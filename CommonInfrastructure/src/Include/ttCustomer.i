
/*------------------------------------------------------------------------
    File        : ttCustomer.i
    Purpose     : 

    Syntax      :

    Description : 

    Author(s)   : 
    Created     : Thu Oct 03 14:55:14 EEST 2019
    Notes       :
  ----------------------------------------------------------------------*/

/* ***************************  Definitions  ************************** */

DEFINE {&ClassAccess} TEMP-TABLE ttCustomer NO-UNDO
BEFORE-TABLE ttCustomerBefore
    field Custnum as integer
    field Country as character
    field name as character
    field Address as character
    field Address2 as character
    field City as character
    field State as character
    field PostalCode as character
    field Contact as character
    field Phone as character
    field SalesRep as character
    field CreditLimit as decimal
    field Balance as decimal
    field Terms as character
    field Discount as integer
    field Comments as character
    field Fax as character
    field EmailAddress as character
    
    index Comments is word-index Comments
    index CountryPost Country PostalCode
    index CustNum is unique primary Custnum
    index name Name
    index SalesRep SalesRep
.

/* ********************  Preprocessor Definitions  ******************** */


/* ***************************  Main Block  *************************** */
