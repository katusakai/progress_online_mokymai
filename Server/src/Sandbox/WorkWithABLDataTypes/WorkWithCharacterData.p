
/*------------------------------------------------------------------------
    File        : WorkWithCharacterData.p
    Purpose     : 

    Syntax      :

    Description : 

    Author(s)   : 
    Created     : Mon Oct 07 08:29:46 EEST 2019
    Notes       :
  ----------------------------------------------------------------------*/

/* ***************************  Definitions  ************************** */

BLOCK-LEVEL ON ERROR UNDO, THROW.


define variable ServiceDoc as character no-undo.

ServiceDoc =  '
<?xml version="1.0" encoding="UTF-8"?>
<wsdl:definitions name="NewCoService" targetNamespace="urn:OpenEdgeServices:NewCoService" xmlns:tns="urn:OpenEdgeServices:NewCoService" xmlns:S2="urn:OpenEdgeServices:NewCoService:NewCoService" xmlns:soapenc="http://schemas.xmlsoap.org/soap/encoding/" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soap="http://schemas.xmlsoap.org/wsdl/soap/" xmlns:S3="urn:OpenEdgeServices:NewCoService:funclib" xmlns:wsdl="http://schemas.xmlsoap.org/wsdl/" xmlns:S1="urn:soap-fault:details" xmlns="http://schemas.xmlsoap.org/wsdl/">
<wsdl:documentation>Author=Progress Education Services, EncodingType=RPC_ENCODED, WSA_Product=10.1A01 - N/A</wsdl:documentation>
  <wsdl:types>
<schema elementFormDefault="unqualified" targetNamespace="urn:soap-fault:details" xmlns="http://www.w3.org/2001/XMLSchema"><complexType name="FaultDetail"><sequence><element name="errorMessage" type="xsd:string"/><element name="requestID" type="xsd:string"/></sequence></complexType></schema>
<schema elementFormDefault="unqualified" targetNamespace="urn:OpenEdgeServices:NewCoService:NewCoService" xmlns="http://www.w3.org/2001/XMLSchema"><import namespace="http://schemas.xmlsoap.org/soap/encoding/"/><complexType name="GetInvoiceTotalInfo_InvoiceTotalsRow"><sequence><element name="invoiceNum" nillable="true" type="xsd:int"/><element name="totalOfOrders" nillable="true" type="xsd:decimal"/><element name="shipCharge" nillable="true" type="xsd:decimal"/><element name="totalDue" nillable="true" type="xsd:decimal"/></sequence></complexType><complexType name="ArrayOfGetInvoiceTotalInfo_InvoiceTotalsRow"><complexContent><restriction base="soapenc:Array"><attribute ref="soapenc:arrayType" wsdl:arrayType="S2:GetInvoiceTotalInfo_InvoiceTotalsRow[]"/></restriction></complexContent></complexType></schema>
<schema elementFormDefault="unqualified" targetNamespace="urn:OpenEdgeServices:NewCoService:funclib" xmlns="http://www.w3.org/2001/XMLSchema"><complexType name="funclibID"><sequence><element name="UUID" type="xsd:string"/></sequence></complexType></schema>
  </wsdl:types>
  <wsdl:message name="NewCoService_GetCustomerNumber">
    <wsdl:part name="cCustName" type="xsd:string"/>
  </wsdl:message>
  <wsdl:message name="NewCoService_SendOrders">
    <wsdl:part name="cDocument" type="xsd:string"/>
  </wsdl:message>
   <wsdl:message name="NewCoService_SendOrdersGetInvoice">
    <wsdl:part name="cDocument" type="xsd:string"/>
  </wsdl:message>
  <wsdl:binding name="funclibObj" type="tns:funclibObj">
    <soap:binding style="rpc" transport="http://schemas.xmlsoap.org/soap/http"/>
    <wsdl:operation name="Release_funclib">
      <soap:operation soapAction="" style="rpc"/>
      <wsdl:input>
        <soap:header message="tns:funclibID" part="funclibID" use="encoded" encodingStyle="http://schemas.xmlsoap.org/soap/encoding/" namespace="urn:OpenEdgeServices:NewCoService:funclib" wsdl:required="true">
        </soap:header>
        <soap:body use="encoded" encodingStyle="http://schemas.xmlsoap.org/soap/encoding/" namespace="urn:OpenEdgeServices:NewCoService:funclib"/>
      </wsdl:input>
      <wsdl:output>
        <soap:body use="encoded" encodingStyle="http://schemas.xmlsoap.org/soap/encoding/" namespace="urn:OpenEdgeServices:NewCoService:funclib"/>
      </wsdl:output>
      <wsdl:fault name="funclibFault">
        <soap:fault name="funclibFault" use="encoded" encodingStyle="http://schemas.xmlsoap.org/soap/encoding/" namespace="http://NTEDUWEBTRAIN/wsa/wsa1"/>
      </wsdl:fault>
    </wsdl:operation>
    <wsdl:operation name="CustomerName">
      <soap:operation soapAction="" style="doc-literal"/>
      <wsdl:input>
        <soap:header message="tns:funclibID" part="funclibID" use="encoded" encodingStyle="http://schemas.xmlsoap.org/soap/encoding/" namespace="urn:OpenEdgeServices:NewCoService:funclib" wsdl:required="true">
        </soap:header>
        <soap:body use="encoded" encodingStyle="http://schemas.xmlsoap.org/soap/encoding/" namespace="urn:OpenEdgeServices:NewCoService:funclib"/>
      </wsdl:input>
      <wsdl:output>
        <soap:body use="encoded" encodingStyle="http://schemas.xmlsoap.org/soap/encoding/" namespace="urn:OpenEdgeServices:NewCoService:funclib"/>
      </wsdl:output>
      <wsdl:fault name="funclibFault">
        <soap:fault name="funclibFault" use="encoded" encodingStyle="http://schemas.xmlsoap.org/soap/encoding/" namespace="http://NTEDUWEBTRAIN/wsa/wsa1"/>
      </wsdl:fault>
    </wsdl:operation>
    <wsdl:operation name="CustomerNumber">
      <soap:operation soapAction="" style="rpc"/>
      <wsdl:input>
        <soap:header message="tns:funclibID" part="funclibID" use="encoded" encodingStyle="http://schemas.xmlsoap.org/soap/encoding/" namespace="urn:OpenEdgeServices:NewCoService:funclib" wsdl:required="true">
        </soap:header>
        <soap:body use="encoded" encodingStyle="http://schemas.xmlsoap.org/soap/encoding/" namespace="urn:OpenEdgeServices:NewCoService:funclib"/>
      </wsdl:input>
      <wsdl:output>
        <soap:body use="encoded" encodingStyle="http://schemas.xmlsoap.org/soap/encoding/" namespace="urn:OpenEdgeServices:NewCoService:funclib"/>
      </wsdl:output>
      <wsdl:fault name="funclibFault">
        <soap:fault name="funclibFault" use="encoded" encodingStyle="http://schemas.xmlsoap.org/soap/encoding/" namespace="http://NTEDUWEBTRAIN/wsa/wsa1"/>
      </wsdl:fault>
    </wsdl:operation>
  </wsdl:binding>
  <wsdl:service name="NewCoServiceService">
    <wsdl:port name="NewCoServiceObj" binding="tns:NewCoServiceObj">
<documentation>This service provides its users with the capability of placing Orders with NewCo and receiving Invoices. It uses an RPC/Encoded binding.</documentation>
      <soap:address location="http://wstraining.progress.com/wsa/wsa1"/>
    </wsdl:port>
    <wsdl:port name="funclibObj" binding="tns:funclibObj">
<documentation></documentation>
      <soap:address location="http://wstraining.progress.com/wsa/wsa1"/>
    </wsdl:port>
  </wsdl:service>
</wsdl:definitions>
'.

/* ********************  Preprocessor Definitions  ******************** */


/* ***************************  Main Block  *************************** */

DEFINE VARIABLE idx as INTEGER NO-UNDO.
DEFINE VARIABLE WorkString AS CHARACTER NO-UNDO.

idx = INDEX (ServiceDoc, "CustomerNumber").
WorkString = SUBSTRING (ServiceDoc, idx).
idx = index (WorkString, "style=").
WorkString = SUBSTRING (WorkString, idx + 7).
idx = INDEX(WorkString, '"').
WorkString = SUBSTRING (WorkString, 1, idx - 1).
Message "Style for operation CustomerNumber is: " WorkString
VIEW-AS ALERT-BOX.
