 
 /*------------------------------------------------------------------------
    File        : OrderDataSource
    Purpose     : 
    Syntax      : 
    Description : 
    Author(s)   : Administrator
    Created     : Mon Oct 07 15:39:10 EEST 2019
    Notes       : 
  ----------------------------------------------------------------------*/

USING Progress.Lang.*.
USING DataSource.IDataSource.

BLOCK-LEVEL ON ERROR UNDO, THROW.

CLASS DataSource.OrderDataSource IMPLEMENTS IDataSource: 
    
    DEFINE DATA-SOURCE OrderSource for Order.
    /*------------------------------------------------------------------------------
     Purpose:
     Notes:
    ------------------------------------------------------------------------------*/
        
    CONSTRUCTOR PUBLIC OrderDataSource (  ):
        SUPER ().
        
    END CONSTRUCTOR.

    METHOD PUBLIC VOID Attach( INPUT phTableBuffer AS HANDLE ):
        
        phTableBuffer:ATTACH-DATA-SOURCE (DATA-SOURCE OrderSource:handle).
        
        RETURN.
    END METHOD.

    METHOD PUBLIC VOID Attach( INPUT phTableBuffer AS HANDLE, INPUT pFilter AS CHARACTER ):
        
        phTableBuffer:ATTACH-DATA-SOURCE (DATA-SOURCE OrderSource:handle).
        data-source OrderSource:FILL-WHERE-STRING = "where " + pFilter.
        
        RETURN.
    END METHOD.

    METHOD PUBLIC VOID Detach( INPUT phTableBuffer AS HANDLE ):
        
        phTableBuffer:DETACH-DATA-SOURCE ().
        
        RETURN.
    END METHOD.

END CLASS.