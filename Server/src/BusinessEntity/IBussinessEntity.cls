
/*------------------------------------------------------------------------
    File        : IBussinessEntity
    Purpose     : 
    Syntax      : 
    Description : 
    Author(s)   : Administrator
    Created     : Thu Oct 03 14:23:03 EEST 2019
    Notes       : 
  ----------------------------------------------------------------------*/

USING Progress.Lang.*.

INTERFACE BusinessEntity.IBussinessEntity:  
  
    /*------------------------------------------------------------------------------
     Purpose:
     Notes:
    ------------------------------------------------------------------------------*/

    METHOD PUBLIC VOID GetData( input pFilter as Character,
                                output phds as HANDLE ).

    /*------------------------------------------------------------------------------
     Purpose:
     Notes:
    ------------------------------------------------------------------------------*/

    METHOD PUBLIC VOID UpdateData( INPUT-OUTPUT phdsChange as HANDLE ).

END INTERFACE.