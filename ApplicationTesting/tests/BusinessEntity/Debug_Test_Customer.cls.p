
/*------------------------------------------------------------------------
    File        : Debug_Test_CustomerBE.cls.p
    Purpose     : Used to debug the test class

    Syntax      :

    Description : 

    Author(s)   : 
    Created     : Thu Oct 08 11:04:27 EDT 2015
    Notes       :
  ----------------------------------------------------------------------*/

/* ***************************  Definitions  ************************** */

block-level on error undo, throw.

using BusinessEntity.Test_CustomerBE.
 
define variable oTest as Test_CustomerBE no-undo.

oTest = new Test_CustomerBE().

oTest:setUp().

/* call all of the test methods */
oTest:testGetData().
/*oTest:testGetOrderData().*/
oTest:testUpdateData().

oTest:tearDown().
delete object oTest.
    
 
 
