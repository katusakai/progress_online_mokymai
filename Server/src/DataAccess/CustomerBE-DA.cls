 
 /*------------------------------------------------------------------------
    File        : CustomerBE-DA
    Purpose     : 
    Syntax      : 
    Description : 
    Author(s)   : Administrator
    Created     : Mon Oct 07 15:46:58 EEST 2019
    Notes       : 
  ----------------------------------------------------------------------*/

USING Progress.Lang.*.
USING DataSource.CustomerDataSource.
USING DataSource.OrderDataSource.
USING DataSource.OrderLineDataSource.

BLOCK-LEVEL ON ERROR UNDO, THROW.

CLASS DataAccess.CustomerBE-DA: 
    
    {include/dsCustomerBE.i &ClassAccess = "private"}
    
    DEFINE PRIVATE VARIABLE CustomerDS AS CustomerDataSource NO-UNDO.
    DEFINE PRIVATE VARIABLE OrderDS AS OrderDataSource NO-UNDO.
    DEFINE PRIVATE VARIABLE OrderLineDS AS OrderLineDataSource NO-UNDO.
    /*------------------------------------------------------------------------------
     Purpose:
     Notes:
    ------------------------------------------------------------------------------*/
        
    CONSTRUCTOR PUBLIC CustomerBE-DA (  ):
        SUPER ().
        
        CustomerDs = new CustomerDataSource().
        OrderDS = new OrderDataSource().
        OrderLineDS = new OrderLineDataSource().
        
    END CONSTRUCTOR.

    /*------------------------------------------------------------------------------
     Purpose:
     Notes:
    ------------------------------------------------------------------------------*/

    METHOD PUBLIC VOID GetAllData( OUTPUT DATASET dsCustomerBE ):
        
        DATASET dsCustomerBE:EMPTY-DATASET ().
        
        CustomerDS:Attach(BUFFER ttCustomer:handle).
        OrderDS:Attach(BUFFER ttOrder:handle).
        OrderLineDS:Attach(Buffer ttOrderLine:handle).
        
        dataset dsCustomerBE:FILL ().
        
        CustomerDS:Detach(buffer ttCustomer:handle).
        OrderDS:Detach(BUFFER ttOrder:HANDLE).
        OrderLineDS:Detach(BUFFER ttOrderLine:HANDLE).
        
        
        RETURN.
    END METHOD.

    /*------------------------------------------------------------------------------
     Purpose:
     Notes:
    ------------------------------------------------------------------------------*/

    METHOD PUBLIC VOID GetCustomerData( INPUT pFilter AS CHARACTER,
                                        OUTPUT DATASET dsCustomerBE ):
        
        DATASET dsCustomerBE:EMPTY-DATASET ().
        
        CustomerDS:Attach(BUFFER ttCustomer:handle, pFilter).
        OrderDS:Attach(BUFFER ttOrder:handle).
        OrderLineDS:Attach(Buffer ttOrderLine:handle).
        
        dataset dsCustomerBE:FILL ().
        
        CustomerDS:Detach(buffer ttCustomer:handle).
        OrderDS:Detach(BUFFER ttOrder:HANDLE).
        OrderLineDS:Detach(BUFFER ttOrderLine:HANDLE).
        
        RETURN.

    END METHOD.

    /*------------------------------------------------------------------------------
     Purpose:
     Notes:
    ------------------------------------------------------------------------------*/

    METHOD PUBLIC VOID GetOrderData( INPUT pFilter AS CHARACTER,
                                     OUTPUT DATASET dsCustomerBE ):
                                         
        DEFINE VARIABLE cNum AS INTEGER NO-UNDO INITIAL 0.
        
        DATASET dsCustomerBE:EMPTY-DATASET ().
        
        CustomerDS:Attach(BUFFER ttCustomer:handle).
        OrderDS:Attach(BUFFER ttOrder:handle, pFilter).
        OrderLineDS:Attach(Buffer ttOrderLine:handle).
        
        dataset dsCustomerBE:FILL ().
        
        CustomerDS:Detach(buffer ttCustomer:handle).
        OrderDS:Detach(BUFFER ttOrder:HANDLE).
        OrderLineDS:Detach(BUFFER ttOrderLine:HANDLE).
        
        IF AVAILABLE (ttOrder)
        THEN
            cNum = ttOrder.CustNum.
        
        FOR EACH ttCustomer WHERE ttCustomer.Custnum <> cNum:
            DELETE ttCustomer.
        END.
        
        
        RETURN.

    END METHOD.

END CLASS.