 
 /*------------------------------------------------------------------------
    File        : Department
    Purpose     : 
    Syntax      : 
    Description : 
    Author(s)   : Administrator
    Created     : Thu Oct 03 08:27:41 EEST 2019
    Notes       : 
  ----------------------------------------------------------------------*/

USING Progress.Lang.*.
USING Sandbox.TestClasses.Employee.

BLOCK-LEVEL ON ERROR UNDO, THROW.

CLASS Sandbox.TestClasses.Department: 

    DEFINE PUBLIC PROPERTY DeptName AS CHARACTER NO-UNDO 
    GET.
    PRIVATE SET. 

    DEFINE PUBLIC PROPERTY ExpenseCode AS CHARACTER NO-UNDO 
    GET.
    PRIVATE SET. 
    
    DEFINE PRIVATE VARIABLE NextEmployeeIndex AS INTEGER NO-UNDO INITIAL 1.
    
    DEFINE PRIVATE VARIABLE Employees AS Employee EXTENT NO-UNDO.

    /*------------------------------------------------------------------------------
     Purpose:
     Notes:
    ------------------------------------------------------------------------------*/
        
    CONSTRUCTOR PUBLIC Department ( INPUT pDeptName AS CHARACTER,
                                    INPUT pMaxNumEmployees AS INTEGER,
                                    INPUT pExpenseCode AS CHARACTER ):
        SUPER ().
        
        ASSIGN
            DeptName = pDeptName
            ExpenseCode = pExpenseCode
            EXTENT (Employees) = pMaxNumEmployees
        .
        
    END CONSTRUCTOR.

    /*------------------------------------------------------------------------------
     Purpose:
     Notes:
    ------------------------------------------------------------------------------*/

    METHOD PUBLIC VOID AddEmployee( INPUT pEmployee AS Employee ):

        ASSIGN
            Employees[NextEmployeeIndex] = pEmployee
            NextEmployeeIndex = NextEmployeeIndex + 1
        .       
        
        RETURN.

    END METHOD.
    
    METHOD PUBLIC VOID AddEmployee ( input pFirstName as CHARACTER,
                                   input pLastName as CHARACTER,
                                   input pJobTitle as CHARACTER,
                                   input pEmpNum as INTEGER,
                                   input pVacationHours as INTEGER,
                                   input pAddress as CHARACTER,
                                   input pPostalCode as CHARACTER,
                                   input pPhoneNumbers as CHARACTER extent 3
                                   ): 
                                       
        DEFINE VARIABLE Emp AS Employee NO-UNDO.
        Emp = new Employee().
        Emp:Initialize(pFirstName, pLastName, pJobTitle, 
                        pEmpNum, pVacationHours, pAddress, 
                        pPostalCode, pPhoneNumbers).
        ASSIGN
            Employees[NextEmployeeIndex] = Emp
            NextEmployeeIndex = NextEmployeeIndex + 1
        .
        
   END METHOD.

    /*------------------------------------------------------------------------------
     Purpose:
     Notes:
    ------------------------------------------------------------------------------*/

    METHOD PUBLIC INTEGER GetLastEmployeeNumber(  ):
        
        DEFINE VARIABLE result AS INTEGER NO-UNDO.
        result = Employees[NextEmployeeIndex - 1]:EmpNum.
        
        RETURN result.

    END METHOD.

    /*------------------------------------------------------------------------------
     Purpose:
     Notes:
    ------------------------------------------------------------------------------*/

    METHOD PUBLIC INTEGER NumberEmployees(  ):
        
        DEFINE VARIABLE result AS INTEGER NO-UNDO.
        result = NextEmployeeIndex - 1.
        
        RETURN result.

    END METHOD.

    /*------------------------------------------------------------------------------
     Purpose:
     Notes:
    ------------------------------------------------------------------------------*/

    METHOD PUBLIC Employee GetEmployee( INPUT pIndex AS INTEGER ):
        
        DEFINE VARIABLE result AS Employee NO-UNDO.
        result = Employees[pIndex].
        RETURN result.

    END METHOD.

    DESTRUCTOR PUBLIC Department ( ):
        define variable i as integer no-undo.
        do i = 1 to NextEmployeeIndex - 1 by 1:
            delete object Employees[i].
        end.
    END DESTRUCTOR.

END CLASS.